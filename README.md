# Kubernetes Cluster Configuration

This hold the main configuration about AGEPoly K8S cluster : 
  - roles
  - gitlab-kubernetes-agent (It need to be there to ensure correct intial deployment)

And also the gitlab agent configuration (`.gitlab/agents/<AGENT-NAME>/config.yaml`)

This is repo that is automatically pulled by the gitlab in-cluster agent without the CI or any other intervention.

## Organization
At the root of the repo, put the k8s manifest that need to be deployed. Put only manifests than can't go elsewhere : ClusterRoles, Roles, GitlabAgent, ...

The special `.gitlab/`folder contains the agents configuration.
## Cluster creation

You should create a new agent config in `.gitlab/agents/<AGENT-NAME>/config.yaml` (Copy it from an existing agent).

(https://docs.gitlab.com/ee/user/clusters/agent/install/index.html)[https://docs.gitlab.com/ee/user/clusters/agent/install/index.html]
You now have to retrieve the agent token on this page with the following steps : 
 - Ensure that GitLab CI/CD is enabled in your project.
 - From your project’s sidebar, select Infrastructure > Kubernetes clusters.
 - Select Actions.
 - From the Select an agent dropdown, select the agent you want to connect and select Register an agent to access the installation form.
- The form reveals your registration token. Securely store this secret token : TODO add storge method.


You should then run : `kubectl apply -f https://gitlab.com/agepoly/it/infra/kubernetes-cluster-configuration/-/raw/main/gitlab-agent.yaml`

You can now add the token to the cluster : `kubectl create secret generic -n gitlab-kubernetes-agent gitlab-agent-token --from-literal=token='<YOUR_AGENT_TOKEN>'`
