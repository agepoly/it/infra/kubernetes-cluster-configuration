apiVersion: apps/v1
kind: Deployment
metadata:
  name: musical-wp
  namespace: legacy
  labels:
    app: musical-wp
spec:
  replicas: 1
  selector:
    matchLabels:
      app: musical-wp
  template:
    metadata:
      labels:
        app: musical-wp
    spec:
      affinity:
        podAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchLabels:
                app: musical-wp
            topologyKey: kubernetes.io/hostname

      containers:
      - name: musical
        image: wordpress:6.3.0-php8.2-apache
        imagePullPolicy: IfNotPresent
        env:
        - name: WORDPRESS_DB_HOST
          valueFrom:
            secretKeyRef:
              key: WORDPRESS_DB_HOST
              name: musical-wp-env-secrets
        - name: WORDPRESS_DB_USER
          valueFrom:
            secretKeyRef:
              key: WORDPRESS_DB_USER
              name: musical-wp-env-secrets
        - name: WORDPRESS_DB_NAME
          valueFrom:
            secretKeyRef:
              key: WORDPRESS_DB_NAME
              name: musical-wp-env-secrets
        - name: WORDPRESS_DB_PASSWORD
          valueFrom:
            secretKeyRef:
              key: WORDPRESS_DB_PASSWORD
              name: musical-wp-env-secrets
        - name: WORDPRESS_CONFIG_EXTRA
          value: |
            define("WP_SITEURL", "https://".explode(":", getenv("HOST"))[1]."/");
            define("WP_HOME",   "https://".explode(":", getenv("HOST"))[1]."/");
        - name: HOST
          value: musical.agepoly.ch:musical.epfl.ch
        
        ports:
        - containerPort: 80
          protocol: TCP
        
        resources: {}

        livenessProbe:
          failureThreshold: 2
          httpGet:
            httpHeaders:
            - name: Host
              value: musical.agepoly.ch
            path: /
            port: 80
            scheme: HTTP
          initialDelaySeconds: 20
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 10
        
        volumeMounts:
        - mountPath: /var/www/html/wp-content/
          name: musical-wordpress-content-v
      
      initContainers:
      - command:
        - sh
        - -c
        - chown -R 33:33 /var/www/html/wp-content/
        image: busybox
        name: init-container
        resources: {}
        
        volumeMounts:
        - mountPath: /var/www/html/wp-content/
          name: musical-wordpress-content-v
      
      volumes:
      - name: musical-wordpress-content-v
        persistentVolumeClaim:
          claimName: musical-wordpress-content-pvc

---
kind: PersistentVolumeClaim
apiVersion: v1
metadata:
  name: musical-wordpress-content-pvc
  namespace: legacy
spec:
  storageClassName: hdd-r3
  accessModes:
  - ReadWriteOnce
  resources:
    requests:
      storage: 2Gi

---
apiVersion: v1
kind: Service
metadata: 
  name: musical-wp
  namespace: legacy 
  labels:
    app: musical-wp
spec:
  ports:
  - name: http
    port: 80
    protocol: TCP
    targetPort: 80
  selector:
    app: musical-wp
  type: ClusterIP

---
apiVersion: gateway.networking.k8s.io/v1beta1
kind: HTTPRoute
metadata:
  name: musical-wp
  namespace: legacy
spec:
  parentRefs:
  - name: vip-240
    sectionName: main
    namespace: istio-system
  hostnames: ["musical.agepoly.ch"]
  rules:
  - matches:
    - path:
        type: PathPrefix
        value: /
    backendRefs:
    - name: musical-wp
      port: 80

---
apiVersion: gateway.networking.k8s.io/v1beta1
kind: HTTPRoute
metadata:
  name: musical-wp-epfl
  namespace: legacy
spec:
  parentRefs:
  - name: vip-240
    sectionName: external-musical-epfl-ch
    namespace: istio-system
  hostnames: ["musical.epfl.ch"]
  rules:
  - matches:
    - path:
        type: PathPrefix
        value: /
    backendRefs:
    - name: musical-wp
      port: 80